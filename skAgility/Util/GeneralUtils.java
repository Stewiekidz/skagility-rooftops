package scripts.skAgility.Util;

import java.util.List;

import org.tribot.api.DynamicClicking;
import org.tribot.api.General;
import org.tribot.api.Timing;
import org.tribot.api.types.generic.Condition;
import org.tribot.api2007.Objects;
import org.tribot.api2007.Player;
import org.tribot.api2007.WebWalking;
import org.tribot.api2007.types.RSObject;

import scripts.skAgility.Data.Obstacles;
import scripts.skAgility.Handler.Antiban;

public class GeneralUtils {

	public static void doObstacles(final List<Obstacles> obstacles) {
		for (Obstacles o : obstacles) {
			if (o.getArea().contains(Player.getPosition())) {

				final RSObject[] OBSTACLE_CLICK = Objects.findNearest(30, o.getId());
				if (OBSTACLE_CLICK != null) {

					if (Player.getPosition().distanceTo(OBSTACLE_CLICK[0]) >= 8) {
						WebWalking.walkTo(o.getTile());
						Timing.waitCondition(new Condition() {

							@Override
							public boolean active() {
								return Player.isMoving();
							}
						}, General.random(500, 1000));
					}
				}

				if (OBSTACLE_CLICK.length > 0) {
					if (OBSTACLE_CLICK[0].isOnScreen()) {
						if (DynamicClicking.clickRSObject(OBSTACLE_CLICK[0], o.getInteract())) {
							Timing.waitCondition(new Condition() {

								@Override
								public boolean active() {
									return Player.getAnimation() != -1 && Player.isMoving();
								}
							}, General.random(1500, 2000));
							General.sleep(500, 1000); // sudo afk
						}
					}

					else {
						if (!OBSTACLE_CLICK[0].isOnScreen()) {
							if (Player.getPosition().distanceTo(
									OBSTACLE_CLICK[0]) >= Antiban.abc.INT_TRACKER.WALK_USING_SCREEN.next()) {
								DynamicClicking.clickRSTile(OBSTACLE_CLICK[0], "Walk here");
								Antiban.abc.INT_TRACKER.WALK_USING_SCREEN.reset();
							} else {
								WebWalking.walkTo(OBSTACLE_CLICK[0]);
							}
						}

						Timing.waitCondition(new Condition() {

							@Override
							public boolean active() {
								return Player.getAnimation() == -1;
							}
						}, General.random(1500, 2000));

					}
				}

			}
		}
	}

}
